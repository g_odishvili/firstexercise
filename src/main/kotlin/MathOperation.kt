import kotlin.math.abs

class MathOperations{

    //greatest common divisor with recursion
    fun gcd(smaller: Int, bigger: Int): Int {
        //performance heavy but it is for safety
        val biggestNumber = maxOf(abs(smaller),abs(bigger))
        val smallestNumber = minOf(abs(smaller),abs(bigger))
        val remainder: Int = biggestNumber % smallestNumber
        return if (remainder == 0) {
            smallestNumber
        } else {
            gcd(smallestNumber, remainder)
        }
    }

    //used gcd for LCM
    fun lcm(smaller: Int, bigger: Int): Int {
        return (smaller * bigger) / gcd(smaller,bigger)
    }

    fun containsDollarSign(stringToBeChecked: String): Boolean{
        return stringToBeChecked.contains('$')
    }

    //passing it as a parameter would have been a better way to go..
    private var amount: Int = 100
    private var sum: Int = 0
    fun sumEven(): Int{
        if(amount == 1){
            return sum
        }

        if(amount % 2 == 0){
          sum+=amount
        }

        amount--

        return sumEven()
    }

    //better version with passed parameter
    fun sumEvenV2(numberToBeSummed: Int): Int{
        if(numberToBeSummed <= 2){
            return numberToBeSummed
        }

        return if(numberToBeSummed % 2 == 0){
            numberToBeSummed + sumEvenV2(numberToBeSummed-1)
        }else{
            sumEvenV2(numberToBeSummed-1)
        }
    }



    fun reverseTheInt(intToBeReverse: Int): Int {
        return intToBeReverse.toString().reversed().toInt()
    }

    fun isPalindrome(palindrome: String): Boolean {
        // replaced empty spaces
        val replaced = palindrome.replace("\\s".toRegex(), "")
        return replaced.equals(replaced.reversed(),true)
    }
}